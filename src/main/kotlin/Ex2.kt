import kotlinx.coroutines.*
import java.util.*

//VERSIÓ 1

/*
fun main() = runBlocking{
    doSalutation()
    println("Finished!")
}

suspend fun doSalutation(){
    println("Introduce la cantidad de mensajes que quieres recibir:")
    val scanner = Scanner(System.`in`)
    val numeroDeMensajes = scanner.nextInt()
    var posicion = 0
    withContext(Dispatchers.Default){
        launch {
            for (i in 1 .. numeroDeMensajes){
                posicion++
                println("$posicion. Hello World!")
                delay(100)
            }
        }
    }
}

 */

//-------------------------------------------------------------------------------------------------------

//VERSIÓ 2

fun main() = runBlocking{
    launch {
        doSalutation()
    }
    println("Finished!")
}

suspend fun doSalutation(){
    println("Introduce la cantidad de mensajes que quieres recibir:")
    val scanner = Scanner(System.`in`)
    val numeroDeMensajes = scanner.nextInt()
    var posicion = 0
    for (i in 1 .. numeroDeMensajes){
        posicion++
        println("$posicion. Hello World!")
        delay(100)
    }
}

