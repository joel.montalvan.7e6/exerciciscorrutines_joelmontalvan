import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Random
import java.util.Scanner

fun main() = runBlocking{
    val randomNumber = 25
    println("You have 10 seconds to guess the secret number...")
    launch {
        guessNumberRandom(randomNumber)
    }
    delay(10000)
    println("The time is up! The secret number was $randomNumber")
}

suspend fun guessNumberRandom(randomNumber: Int){
    var salida = false
    do{
        print("Enter a number:")
        val scanner = Scanner(System.`in`)
        val numberPropose = scanner.nextInt()
        if (numberPropose == randomNumber){
            println("You got it!")
            salida = true
        }
        else println("Wrong number!")
    }while (!salida)

}


