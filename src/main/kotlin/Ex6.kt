import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.random.Random

suspend fun main(){
    println("INICIO DE CARRERA")
    coroutineScope {
        launch{
            cavall1()
        }
        launch {
            cavall2()
        }
        launch {
            cavall3()
        }
        launch {
            cavall4()
        }
        launch {
            cavall5()
        }
    }
    println("\nCARRERA FINALIZADA")
}

suspend fun cavall1() = coroutineScope {
    launch{
        for (i in 5 downTo  1){
            println("$i. Caballo 1 esta a ${i}00 metros de la meta!!!")
            delayRandom()
        }
        println("\nCABALLO 1 HA ACABADO LA CARRERA\n")
    }
}

suspend fun cavall2() = coroutineScope {
    launch{
        for (i in 5 downTo  1){
            println("$i. Caballo 2 esta a ${i}00 metros de la meta!!!")
            delayRandom()
        }
        println("\nCABALLO 2 HA ACABADO LA CARRERA\n")
    }
}

suspend fun cavall3() = coroutineScope {
    launch{
        for (i in 5 downTo  1){
            println("$i. Caballo 3 esta a ${i}00 metros de la meta!!!")
            delayRandom()
        }
        println("\nCABALLO 3 HA ACABADO LA CARRERA\n")
    }
}

suspend fun cavall4() = coroutineScope {
    launch{
        for (i in 5 downTo  1){
            println("$i. Caballo 4 esta a ${i}00 metros de la meta!!!")
            delayRandom()
        }
        println("\nCABALLO 4 HA ACABADO LA CARRERA\n")
    }
}

suspend fun cavall5() = coroutineScope {
    launch{
        for (i in 5 downTo  1){
            println("$i. Caballo 5 esta a ${i}00 metros de la meta!!!")
            delayRandom()
        }
        println("\nCABALLO 5 HA ACABADO LA CARRERA\n")
    }
}

fun delayRandom(){
    val minDelayMillis:Long = 1000
    val maxDelayMillis:Long = 5000
    val random = Random(System.currentTimeMillis())
    val delayMillis = random.nextLong(minDelayMillis, maxDelayMillis)
    Thread.sleep(delayMillis)
}
