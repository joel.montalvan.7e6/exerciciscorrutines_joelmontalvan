import kotlinx.coroutines.*

/* EXERCICI 1:

    1. The main program is started
    2. The main program continues
    3. Background processing started
    1 segon
    4. Background processing finished
    1,5 segon
    5. The main program is finished

 */

// ---------------------------------------------------------------------------------------------------------------------

// EXERCICI 1.1

/*
@OptIn(DelicateCoroutinesApi::class)
fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(900)
        println("The main program is finished")
    }
}

 */

/*
    1. The main program is started
    2. The main program continues
    3. Background processing started
    1 segon
    4. The main program is finished
 */

// ---------------------------------------------------------------------------------------------------------------------


// EXERCICI 1.2

/*
fun main() = runBlocking {
    doBackground()
    delay(1500)
    println("The main program is finished")
}

suspend fun doBackground () = coroutineScope {
    println("The main program is started")
    launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
}

 */

// ---------------------------------------------------------------------------------------------------------------------

// EXERCICI 1.3

/*
fun main() = runBlocking {
    doBackground()
    delay(1500)
    println("The main program is finished")
}

suspend fun doBackground() {
    println("The main program is started")
    println("The main program continues")
    withContext(Dispatchers.Default) {
        launch {
            println("Background processing started")
            delay(1000)
            println("Background processing finished")
        }
    }
}

 */

// ---------------------------------------------------------------------------------------------------------------------





